import sensitive
from webapp.models import db
from oshipka.persistance import user_datastore


def populate_db(app):
    with app.app_context():
        for user in sensitive.users:
            user_datastore.create_user(email=user[0], password=user[1])
        db.session.commit()
