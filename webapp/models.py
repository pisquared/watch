from oshipka.persistance import db, ModelController, Ownable


class Video(db.Model, ModelController, Ownable):
    title = db.Column(db.Unicode)
    filename = db.Column(db.Unicode)
