from os import listdir

from flask import render_template, send_from_directory
from flask_security import login_required
from oshipka.persistance import db

from oshipka.webapp import app

from config import MEDIA_DIR
from webapp.models import Video


def index_videos():
    all_videos = Video.query.all()
    fnames_videos = {v.filename: v for v in all_videos}
    keep_videos = set()
    for f in listdir(MEDIA_DIR):
        if f not in fnames_videos:
            video = Video(title=f, filename=f)
            db.session.add(video)
        else:
            keep_videos.add(f)
    for f in set(fnames_videos.keys()) - keep_videos:
        db.session.delete(fnames_videos[f])
    db.session.commit()


@app.route('/')
@login_required
def home():
    index_videos()
    videos = Video.query.all()
    return render_template("home.html", videos=videos)


@app.route('/watch/<int:video_id>')
@login_required
def watch(video_id):
    video = Video.query.filter_by(id=video_id).first()
    return render_template("watch.html", filename=video.filename)


@app.route('/media/<path:filename>')
@login_required
def serve_media(filename):
    return send_from_directory(MEDIA_DIR, filename)
