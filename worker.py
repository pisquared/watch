from oshipka import worker, init_db

from populate import populate_db
from webapp.app import app

if init_db(app):
    populate_db(app)


if __name__ == "__main__":
    worker.main()
